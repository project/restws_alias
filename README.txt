------------------------------------------------------------------------------
                REST WS Alias (restws_alias)
------------------------------------------------------------------------------

INTRODUCTION
------------
 This module allows RESTful Webservices (using restws module) to be
 accessible through url aliases in addition to just the direct path.

Read more discussion here: https://www.drupal.org/node/1311788

Example Exports
---------------
 For example: If you have a node with direct path "node/1" and url alias
 "page/awesomepage", the restws module allows you to access json representation
 of the node through "node/1.json" and not "page/awesomepage.json".

 This module allows you to access the node through "page/awesomepage.json" by
 adding new url aliases such as:
 page/awesomepage.json ==> node/1.json
 page/awesomepage.xml ==> node/1.xml
 page/awesomepage.rdf ==> node/1.rdf

REQUIREMENTS
------------
This module requires the following modules:
  Path (https://drupal.org/project/path) : A part of Drupal core in Drupal 7
  Restws (https://drupal.org/project/restws)

INSTALLATION
------------
  Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
 No module specific configuration needed.
 However, To generate these aliases for the nodes already existing in the
 system, please bulk update the aliases by going to:
 Configuration -> URL aliases. Then Delete and Bulk update in succession.
 
MAINTAINERS
-----------
Current maintainers:
 Jaskaran Nagra - https://www.drupal.org/u/jaskaran.nagra
------------------------------------------------------------------------------
